#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <stdlib.h>


// Number of response our robot can make
#define NUM_RESPONSE 5

// Input the robot can recognize
char *INPUT_STR[] = {"Good", 
                     "Leaving", 
                     "Age", 
                     "Weather", 
                     "Sports"};

// Corresponding response the robot should make
char *RESPONSE_STR[] = {"Good day to you as well.", 
                        "Good bye.", 
                        "I am a robot and I am 4 hours old.", 
                        "It's raining.", 
                        "Go Ducks!",
                        "I don't understand what you are saying."};


void arg_test(int argc, char **argv);
char **str_return(int argc, char **argv);
int str_comparison(char *str1, char *str2);
char *str_case(int str_length, char *str);
void print_input();
void print_response();
void respond(int argc, char **argv);

int main(int argc, char **argv)
{
    // Sample code 
    arg_test(argc, argv);
    // Homework 1, Part 1
    respond(argc, argv);

    return 0;
}



void arg_test(int argc, char **argv)
{
    if(argc < 2) {
        fprintf(stderr, "Error: no input entered\n");
        fprintf(stderr, "usage: %s <some input string>\n", argv[0]);
        fprintf(stderr, "\n");
    } else {
        // Do nothing

        // I am not sure if we actually get no argvs in the reality. 
        // If so, would it cause the program crash since I did not add any condition determination 
        // in my response function. 
    }

}

char **str_return(int argc, char **argv)
{
    printf("# input (excluding the command): %d\n", argc - 1);
    // i starts from 1 as to skip the command
    char **str;
    for(int i = 1; i < argc; i++) {
        uint32_t length = strlen(argv[i]);
        // enter each character to the str from the argv one at a time
        for(int j = 0; j < length; j++) {
            str[i][j] = argv[i][j];
        }
        
    }
    return str;
}

int str_comparison(char *str1, char *str2)
{
    // Here we test out string comparison
    // int comp = strcmp("Hello", "hello");
    
    // compare both strings by strcmp()
    int comp = strcmp(str1, str2);
    if (comp != 0) {
        // when the strings are differnt, return 0
        int boo = 0;
        return boo;
    }
    else
    {
        // when strings are same, return 1
        int boo = 1;
        return boo;
    }
    
    
    
}

char *str_case(int length, char *str)
{   
    // allocate the memory for the new string which we are going to return. 
    char *dest = (char*)malloc((length + 1) * sizeof(char));
    strcpy(dest, str);
    for(int i = 0; i < length; i++)  {
        //change all the characters to upper case
        dest[i] = tolower(dest[i]); 
    }
    // return the transformed string
    return dest;
}

void print_input()
{
    printf("---- Recognized Input ----\n");
    for(int i = 0; i < NUM_RESPONSE; i++) {
        printf("%d ::: %s\n", i, INPUT_STR[i]);
    }
    printf("--------\n\n");
}

void print_response()
{
    printf("---- Response ----\n");
    // Note that we are printing NUM_RESPONSE + 1 because of the
    // last undefined input response "I don't understand what you are saying."
    for(int i = 0; i < NUM_RESPONSE + 1; i++) {
        printf("%d ::: %s\n", i, RESPONSE_STR[i]);
    }
    printf("--------\n\n");
}

// This function should take in the input from the user
// and respond appropriately depending on whether the
// input contains a recognizable word (i.e., INPUT_STR_1~5);
// The apppropriate response to INPUT_STR[1] is RESPONSE_STR[1]
// and so on.
// If the input does not contain any of the recognizable words,
// respond with "I don't understand what you are saying."
// NOTE 1:
// The input can be in lower case, upper case, or any combination
// thereof. Be sure to account for that!
// NOTE 2:
// If a sentence contains multiple recognizable words, you should
// make multiple appropriate responses.
void respond(int argc, char **argv)
{
    printf("---- Answer ----\n");
    // set a counter to determine whether we will face a word that can be recognizable. 
    int count = 0;
    // The for loop starts from 1 since the first entry is ./a.out which we should skip.
    for(int i = 1; i < argc; i++)
    {   
        // set comparisons to find whether the entry is recognizable or not.
        
        for(int j = 0; j < NUM_RESPONSE; j++) {
            char *ptr = str_case(strlen(argv[i]), argv[i]);
            char *ptr2 = str_case(strlen(INPUT_STR[j]), INPUT_STR[j]);
            int newBOO = str_comparison(ptr, ptr2);
            if (newBOO == 1) {
                printf("%s\n", RESPONSE_STR[j]);
                count += 1;
            }
            free(ptr);
            free(ptr2);
        }
    }
    // If none of entries can be recognizable, print "I don`t understand".
    if (count == 0) {
        printf("%s\n", RESPONSE_STR[5]);
    }
    printf("--------\n\n");
}

