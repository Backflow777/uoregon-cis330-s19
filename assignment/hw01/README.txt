I got a really weird bug.
I tested my program by lldb, then It ran perfectly. No bugs.
Then I used gcc to compile, then just ran the program on my mac. Then werid thing happened.
If "Leaving" appeared as 2nd term of the argv, then the term could not be recognized. 
If I put it in other place except 2nd place of the argv, the program ran perfectly.
I tested the program on ix-dev, it also worked perfectly.
I guess fixing this bug is beyond my capability.
____________________________________________________

FIXED


Author: Danmeng Wang
danmengw@uoregon.edu
