//
// Implementation of methods for classes Response, AngryResponse, HappyResponse
//
#include <iostream>
#include <string>
#include <algorithm>

#include "response.hh"

using namespace std;

//
// Implementation of Word methods
//
// Don't worry too hard about these implementations.
// If you'd like to learn more about STL see: https://www.geeksforgeeks.org/the-c-standard-template-library-stl/
//
string Word::upper()
{
  string result(theWord);
  transform(result.begin(), result.end(), result.begin(), ::toupper);
  return result;
}

string Word::lower()
{
  string result(theWord);
  transform(result.begin(), result.end(), result.begin(), ::tolower);
  return result;
}

//
// Implementation of Response methods
//
bool Response::checkAndRespond(const string &inWord, ostream &toWhere)
{
  // TODO:
  // This method should check if its object's keyword is in the inWord message.
  // If it is, the method should call the  `respond` method passing the toWhere stream and return true.
  // If it isn't, the method should return false.
  transform(response.begin(), response.end(), response.begin(), ::toupper);
  //printf("This is response %s now\n", response.c_str());
  //printf("This is new_str %s\n", new_str.c_str());
  size_t found = inWord.find(keyword.upper());
  size_t a_found = inWord.find(response);
  // since I assume both first two words in line are "keyword", however the second one is always in
  // the object's response. I have to check both "keyword" and "response", just in order to make no
  // matter which one users type in, it will be checked.
  if (found != string::npos)
  {
    respond(toWhere);
    return true;
  }
  else if (a_found != string::npos)
  {
    respond(toWhere);
    return true;
  }
  else{
    return false;
  }
}

void Response::respond(ostream &toWhere)
{
  
  // This method should insert its object's response into the given stream
  // I got a little confused here, since it wants the object`s response, then it leads
  // wheneven I type "age" or "hours" it will print the current response. so I changed it to
  // ":|" face.
  toWhere << ":|" << endl;

}


//
// Implementation of AngryReponse methods
//

// TODO:
// Implement the `respond` method for the AngryResponse class so that some angry expression (e.g. ":(")
// is included in the output.
void AngryResponse::respond(ostream &toWhere)
{
  // Simple sad face here
  toWhere << ":(" << endl;
}
//
// Implementation of HappyResponse methods
//

// Implement the method(s) for the HappyResponse class.
// Unlike the AngryResponse class, the HappyResponse class should include some happy expression (e.g. ":)")
// in its output.
void HappyResponse::respond(ostream &toWhere)
{
  // Simple smile face here 
  toWhere << ":)" << endl;
}