#include <iostream>

class Leff {

  int *stuff;
  static int index;


public:
  Leff() { stuff = new int[10]; }
  ~Leff() { delete[] stuff; }


  friend Leff &
  operator<<(Leff &left_value, const int &right_value);

  friend std::ostream &
  operator<<(std::ostream &left_value, Leff &right_value);

};


Leff &
operator<<(Leff &left_value, const int &right_value);

int
Leff::index = 0;

Leff &
operator<<(Leff &left_value, const int &right_value) {
  left_value.stuff[Leff::index++] = right_value;
  return left_value;
}

std::ostream &
operator<<(std::ostream &left_value, Leff &right_value)
{
  int i;
  for (i = 0; i < Leff::index; i++) {
    left_value << right_value.stuff[i] << " ";
  }
  return left_value;
}


int
main()
{
  Leff l;


  l << 10;
  l << 20;

  (std::cout << l) << std::endl;

  return 0;
}